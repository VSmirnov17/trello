import React from 'react';
import { BoardList } from '../../components/Boards/BoardList/BoardList';
import { BoardAddPanel } from '../../components/Boards/BoardAddPanel/BoardAddPanel';
import { connect } from 'react-redux';
import { createBoard, fetchBoardList } from '../../redux/actions/board';

class Boards extends React.Component {

    saveBoardHandler(board) {
        this.props.createBoard(board);
    }

    componentDidMount() {
        this.props.fetchBoardList();
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-6">
                    <BoardAddPanel saveBoardHandler={this.saveBoardHandler.bind(this)}/>
                </div>
                <div className="col-md-6">
                    <BoardList list={this.props.list} loading={this.props.loading}/>
                </div>
            </div>
        );
    }
}

function mapStoreToProps(store) {
    return {
        list: store.board.list,
        loading: store.board.loading
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchBoardList: () => dispatch(fetchBoardList()),
        createBoard: (board) => dispatch(createBoard(board))
    };
}

export default connect(mapStoreToProps, mapDispatchToProps)(Boards);
