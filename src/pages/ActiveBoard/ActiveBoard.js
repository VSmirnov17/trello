import React from 'react';
import { connect } from 'react-redux';
import { addTask, createTaskList, fetchBoardById } from '../../redux/actions/board';
import { Loader } from '../../components/UI/Loader/Loader';
import { AddListPanel } from '../../components/Tasks/TaskList/AddListPanel/AddListPanel';
import { TaskList } from '../../components/Tasks/TaskList/TaskList';
import cloneDeep from 'lodash/cloneDeep';
import { DragDropContext } from 'react-beautiful-dnd';

class ActiveBoard extends React.Component {

    state = {
        id: null
    };

    constructor(props) {
        super(props);

        this.patchTasksHandler = this.patchTasksHandler.bind(this);
        this.createTaskListHandler = this.createTaskListHandler.bind(this);
    }

    componentDidMount() {
        this.props.fetchBoardById(this.props.match.params.id);
        this.setState({id: this.props.match.params.id});
    }

    createTaskListHandler(nameList) {
        this.props.createTaskList(this.state.id, nameList);
    }

    patchTasksHandler(taskList) {
        this.props.patchTasks(this.state.id, taskList);
    }

    move = (source, destination, droppableSource, droppableDestination) => {
        const sourceClone = Array.from(source);
        const destClone = Array.from(destination);
        const [removed] = sourceClone.splice(droppableSource.index, 1);

        destClone.splice(droppableDestination.index, 0, removed);

        const result = {};
        result[droppableSource.droppableId] = sourceClone;
        result[droppableDestination.droppableId] = destClone;

        return result;
    };

    reorder = (list, startIndex, endIndex) => {
        const result = Array.from(list);
        const [removed] = result.splice(startIndex, 1);
        result.splice(endIndex, 0, removed);

        return result;
    };

    getList = id => ([...this.props.activeBoard.taskList])[id];

    onDragEnd = result => {
        const {source, destination} = result;
        const taskList = cloneDeep(this.props.activeBoard.taskList);

        if (!destination) {
            return;
        }

        if (source.droppableId === destination.droppableId) {
            taskList[source.droppableId].tasks = this.reorder(
                this.getList(source.droppableId).tasks,
                source.index,
                destination.index
            );
        } else {
            const result = this.move(
                this.getList(source.droppableId).tasks || [],
                this.getList(destination.droppableId).tasks || [],
                source,
                destination
            );

            taskList[source.droppableId].tasks = result[source.droppableId];
            taskList[destination.droppableId].tasks = result[destination.droppableId];
        }

        this.props.patchTasks(this.state.id, taskList);
    };

    render() {
        if (this.props.loading && !this.props.activeBoard.label) {
            return (
                <Loader/>
            );
        }

        return (
                <>
                    <h1 className="text-center bg-success">{this.props.activeBoard.label}</h1>
                    <div className="row justify-content-center">
                        <DragDropContext onDragEnd={this.onDragEnd}>
                            {
                                this.props.activeBoard &&
                                <TaskList list={this.props.activeBoard.taskList}
                                          patchTaskList={this.patchTasksHandler}
                                          classItem="col-md-4"/>
                            }
                        </DragDropContext>

                        <div className="col-md-4">
                            <AddListPanel
                                activeBoard={this.props.activeBoard}
                                createTaskList={this.createTaskListHandler}/>
                        </div>

                    </div>
                </>
        );
    }
}

function mapStateToProps(state) {
    return {
        activeBoard: state.board.activeBoard,
        loading: state.board.loading
    };
}

function mapDispatchToProps(dispatch) {
    return {
        fetchBoardById: id => dispatch(fetchBoardById(id)),
        createTaskList: (id, nameList) => dispatch(createTaskList(id, nameList)),
        patchTasks: (id, taskList) => dispatch(addTask(id, taskList))
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ActiveBoard);
