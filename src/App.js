import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import './App.scss';
import { Header } from './components/Header/Header';
import Boards from './pages/Boards/Boards';
import ActiveBoard from './pages/ActiveBoard/ActiveBoard';

function App() {
    return (
        <>
            <Header/>
            <div className="container">
                <Switch>
                    <Route path="/boards" exact component={Boards}/>
                    <Route path="/boards/:id" component={ActiveBoard}/>
                    <Redirect to="/boards"/>
                </Switch>
            </div>
        </>
    );
}

export default App;
