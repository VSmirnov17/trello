import axios from 'axios';

export default axios.create({
    baseURL: 'https://trello-da270.firebaseio.com/'
});
