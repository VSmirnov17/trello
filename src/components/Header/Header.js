import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSmile } from '@fortawesome/free-solid-svg-icons';
import { withRouter } from 'react-router-dom';

export const Header = withRouter(({history}) => {

    const onClickHandler = () => history.push('/boards');

    return (
        <nav
            className="navbar navbar-expand-lg navbar-light bg-success mb-2 d-flex align-items-center justify-content-center">
            <div className="navbar-brand">
                <FontAwesomeIcon size="3x"
                                 color="white"
                                 icon={faSmile}
                                 style={{cursor: 'pointer'}}
                                 onClick={onClickHandler}
                />
            </div>
        </nav>
    );
});
