import React from 'react';

const Alert = ({alert, closeAlert}) => {
    return (
        <div className="alert alert-danger mb-1 mt-1" role="alert">
            {alert}
            <button type="button"
                    className="close"
                    data-dismiss="alert"
                    aria-label="Close"
                    onClick={() => closeAlert()}>
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    );
};

export default Alert;
