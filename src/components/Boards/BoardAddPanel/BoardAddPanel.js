import React, { useState } from 'react';
import styles from './BoardAddPanel.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import Alert from '../../Alert/Alert';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);

export const BoardAddPanel = ({saveBoardHandler}) => {
    const initBoardState = {
        label: '',
        tasks: []
    };

    const [opened, setOpened] = useState(false);
    const [noNameError, setNoNameError] = useState(false);
    const [board, setBoard] = useState(initBoardState);

    const createBoard = (board) => {
        setNoNameError(false);
        if (board.label.trim() !== '') {
            saveBoardHandler(board);

            setOpened(false);
            setBoard(initBoardState);
        } else {
            setNoNameError(true);
        }
    };

    const onChangeHandler = event => {
        setBoard({...board, label: event.target.value});
    };

    const closeAlertHandler = () => {
        setNoNameError(false);
    }

    const iconCls = cx({
        iconRotate: opened
    });

    const errorCls = cx({
        noNameError: noNameError,
        'w-100': true,
        'p-1': true
    });

    return (
        <div className="BoardAddPanel">
            <div className="panelHeader bg-success p-2 text-white"
                 onClick={() => setOpened(!opened)}>
                <div className="d-flex align-items-center justify-content-between">
                    <h3 className="font-weight-bold m-0">Новая доска</h3>

                    <FontAwesomeIcon icon={faPlus} size="2x"
                                     className={iconCls}
                    />
                </div>
            </div>

            {opened ?
                <div className="panelBody p-3 border-success border-left border-right border-bottom mb-3">
                    <h5>Название доски</h5>

                    {noNameError && <Alert alert="Имя не может быть пустым"
                                           closeAlert={closeAlertHandler}/>}

                    <input type="text"
                           className={errorCls}
                           onChange={event => onChangeHandler(event)}/>

                    <div className="d-flex align-items-center justify-content-between mt-3">
                        <button className="btn btn-secondary"
                                onClick={() => setOpened(false)}>
                            Отмена
                        </button>

                        <button className="btn btn-success"
                                onClick={() => createBoard(board)}>
                            Сохранить
                        </button>
                    </div>
                </div>
                : null
            }
        </div>
    );
};
