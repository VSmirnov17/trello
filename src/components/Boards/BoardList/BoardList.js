import React from 'react';
import './BoardList.scss';
import { NavLink } from 'react-router-dom';
import { Loader } from '../../UI/Loader/Loader';

export const BoardList = ({list, loading}) => {
    return (
        <div className="boardList d-flex flex-column align-items-center">
            {
                loading ? <Loader/> : list.map(item => (
                    <NavLink key={item.id}
                             to={`/boards/${item.id}`}
                             style={{textDecoration: 'none', width: '100%', marginBottom: '5px'}}>
                        <div
                            className="card d-flex align-items-center justify-content-center bg-light align-items-center">
                            <h1>{item.label}</h1>
                        </div>
                    </NavLink>
                ))
            }
        </div>

    );
};
