import React from 'react';

const AddTaskPanel = ({keyDownHandler, indexList}) => {
    return (
        <div>
            <input type="text" className="w-100 p-1 mt-1"
                   onKeyDown={event => keyDownHandler(indexList, event)}
                   placeholder="Введите название задачи"/>
        </div>
    );
};

export default AddTaskPanel;
