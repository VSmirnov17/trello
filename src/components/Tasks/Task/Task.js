import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import styles from './Task.scss';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);

const Task = ({done, label, indexList, indexTask, onClickHandler}) => {
    const taskClass = cx({
        task: true,
        done: done,
        doing: !done,
        border: true,
        'border-success': true,
        'mb-md-1': true
    });

    return (
        <div className={taskClass} onClick={() => onClickHandler(indexList, indexTask)}>
            <h6>{label}</h6>
            <FontAwesomeIcon icon={faCheck}/>
        </div>
    );
};

export default Task;
