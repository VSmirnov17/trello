import React, { Fragment } from 'react';
import cloneDeep from 'lodash/cloneDeep';
import styles from './TaskList.scss';
import { Draggable, Droppable } from 'react-beautiful-dnd';
import TaskListEmpty from './TaskListEmpty/TaskListEmpty';
import Task from '../Task/Task';
import TaskListHeader from './TaskListHeader/TaskListHeader';
import AddTaskPanel from '../Task/AddTaskPanel/AddTaskPanel';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);

export const TaskList = ({patchTaskList, list, classItem}) => {
    const cloneList = list ? cloneDeep(list) : [];

    const keyDownHandler = (index, event) => {
        if (event.key === 'Enter') {
            if (event.target.value.trim() !== '') {
                const taskList = cloneList[index];
                const newTask = {
                    label: event.target.value,
                    done: false
                };

                if (taskList.tasks) {
                    taskList.tasks.push(newTask);
                } else {
                    taskList.tasks = [newTask];
                }

                event.target.value = null;
                patchTaskList(cloneList);
            }
        }
    };

    const onClickDoneHandler = (indexList, indexTask) => {
        const patchedTask = cloneList[indexList].tasks[indexTask];

        patchedTask.done = !patchedTask.done;

        patchTaskList(cloneList);
    };

    const onDeleteListHandler = indexList => {
        cloneList.splice(indexList, 1);
        patchTaskList(cloneList);
    };

    const listCls = cx({
        taskList: true,
        border: true,
        'border-success': true,
        'mb-3': true,
        'p-2': true
    });

    return (
        <Fragment>
            {cloneList.map((item, indexList) => (
                <div key={indexList} className={`${classItem}`}>
                    <div className={listCls}>
                        <TaskListHeader {...item}
                                        indexList={indexList}
                                        onDelete={onDeleteListHandler}/>
                        <div className="d-flex flex-column">
                            <Droppable droppableId={`${indexList}`}>
                                {provided => (
                                    <div ref={provided.innerRef}>
                                        {
                                            item.tasks ?
                                                item.tasks.map((task, indexTask) => (
                                                    <Draggable draggableId={`${indexList}${indexTask}`}
                                                               key={indexTask}
                                                               index={indexTask}>
                                                        {provided => (
                                                            <div ref={provided.innerRef}
                                                                 {...provided.draggableProps}
                                                                 {...provided.dragHandleProps}>

                                                                <Task {...task}
                                                                      indexList={indexList}
                                                                      indexTask={indexTask}
                                                                      onClickHandler={onClickDoneHandler}/>
                                                            </div>
                                                        )}
                                                    </Draggable>
                                                ))
                                                :
                                                <TaskListEmpty/>
                                        }
                                        {provided.placeholder}
                                    </div>
                                )}
                            </Droppable>

                            <AddTaskPanel indexList={indexList}
                                          keyDownHandler={keyDownHandler}/>
                        </div>
                    </div>
                </div>
            ))}
        </Fragment>
    );
};
