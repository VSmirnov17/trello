import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import './TaskListHeader.scss';

const TaskListHeader = ({label, onDelete, indexList}) => {
    return (
        <div className="header">
            <span>{label}</span>
            <FontAwesomeIcon icon={faPlus}
                             size="2x"
                             onClick={() => onDelete(indexList)}
                             className="iconClose"/>
        </div>
    );
};

export default TaskListHeader;
