import React, { useState } from 'react';
import styles from './AddListPanel.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import Alert from '../../../Alert/Alert';
import classNames from 'classnames/bind';

const cx = classNames.bind(styles);

export const AddListPanel = ({createTaskList}) => {
    const [opened, setOpened] = useState(false);
    const [noNameError, setNoNameError] = useState(false);

    const keyDownHandler = event => {
        if (event.key === 'Enter') {
            setNoNameError(false);

            if (event.target.value.trim() !== '') {
                createTaskList(event.target.value);
                setOpened(false);
            } else {
                setNoNameError(true);
            }
        }
    };

    const closeAlertHandler = () => {
        setNoNameError(false);
    };

    const errorCls = cx({
        noNameError: noNameError,
        'p-1': true
    });

    return (
        <div className="taskAddPanel">
            {
                !opened ?
                    <button className="btn btn-block btn-secondary" onClick={() => setOpened(true)}>
                        Добавить список
                    </button>
                    :
                    <div className="panelAdd border border-success p-2">
                        <div className="d-flex align-items-center justify-content-between">
                            <h4 className="m-0">Добавление списка</h4>
                            <FontAwesomeIcon icon={faPlus} size="2x"
                                             className="iconClose"
                                             onClick={() => setOpened(false)}
                            />
                        </div>
                        {noNameError && <Alert alert="Имя не может быть пустым"
                                               closeAlert={closeAlertHandler}/>}
                        <input type="text"
                               className={errorCls}
                               onKeyDown={keyDownHandler}
                               placeholder="Введите название списка"/>
                    </div>
            }
        </div>
    );
};
