import React from 'react';

const TaskListEmpty = () => {
    return (
        <div className="text-center">
            <h5 className="mb-0 p-1">Список задач пуст</h5>
        </div>
    );
};

export default TaskListEmpty;
