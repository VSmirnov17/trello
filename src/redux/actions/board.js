import {
    CREATE_BOARD,
    FETCH_ACTIVE_BOARD_START,
    FETCH_ACTIVE_BOARD_SUCCESS,
    FETCH_BOARD_LIST_SUCCESS,
    PATCH_TASK_LIST_FINISH,
    PATCH_TASK_LIST_START
} from '../types';
import axios from '../../axios/axios-config';

export function addBoard(board) {
    return {
        type: CREATE_BOARD,
        board
    };
}

export function createBoard(board) {
    return async dispatch => {
        try {
            await axios.post('boards.json', board);
            dispatch(addBoard(board));
            dispatch(fetchBoardList());
        } catch (e) {
            console.log(e);
        }
    }
}

export function fetchBoardListStart() {
    return {
        type: FETCH_ACTIVE_BOARD_START
    }
}

export function fetchBoardListSuccess(list) {
    return {
        type: FETCH_BOARD_LIST_SUCCESS,
        list
    }
}

export function fetchBoardList() {
    return async dispatch => {
        dispatch(fetchBoardListStart())
        try {
            const response = await axios.get('boards.json');
            const list = [];

            if (!!response.data) {
                Object.keys(response.data).forEach((key, index) => {
                    const item = response.data[key];
                    list.push({id: key, label: item.label, taskList: item.taskList || []});
                });
            }

            dispatch(fetchBoardListSuccess(list));
        } catch (e) {
            console.log(e);
        }
    };
}

export function fetchBoardSuccess(activeBoard) {
    return {
        type: FETCH_ACTIVE_BOARD_SUCCESS,
        activeBoard
    }
}

export function fetchBoardByIdStart() {
    return {
        type: FETCH_ACTIVE_BOARD_START
    }
}

export function fetchBoardById(id) {
    return async dispatch => {
        dispatch(fetchBoardByIdStart());

        try {
            const response = await axios.get(`boards/${id}.json`);
            const activeBoard = response.data;

            dispatch(fetchBoardSuccess(activeBoard));
        } catch (e) {
            console.log(e);
        }
    };
}

export function patchTaskListFinish(taskList) {
    return {
        type: PATCH_TASK_LIST_FINISH,
        taskList
    }
}

export function createTaskList(id, nameList) {
    return async (dispatch, getState) => {
        const newTaskList = { label: nameList, tasks: []};
        const taskList = [...getState().board.activeBoard.taskList || [], newTaskList];
        try {
            const response = await axios.patch(`boards/${id}/.json`, {taskList});
            dispatch(patchTaskListFinish(response.data));
        } catch (e) {
            console.log(e);
        }
    }
}

export function pathTaskListStart() {
    return {
        type: PATCH_TASK_LIST_START
    }
}

export function addTask(id, taskList) {
    return async dispatch => {
        dispatch(pathTaskListStart());

        try {
            const response = await axios.patch(`boards/${id}/.json`, {taskList});

            dispatch(patchTaskListFinish(response.data));
        } catch (e) {
            console.log(e);
        }
    }
}
