import {
    CREATE_BOARD,
    FETCH_ACTIVE_BOARD_START,
    FETCH_ACTIVE_BOARD_SUCCESS,
    FETCH_BOARD_LIST_START,
    FETCH_BOARD_LIST_SUCCESS,
    PATCH_TASK_LIST_FINISH,
    PATCH_TASK_LIST_START
} from '../types';

const initialState = {
    list: [],
    activeBoard: {
        label: '',
        taskList: []
    },
    loading: false
};

const handlers = {
    [CREATE_BOARD]: (state, {board}) => ({...state, list: [...state.list, board]}),
    [FETCH_BOARD_LIST_START]: state => ({...state, loading: true}),
    [FETCH_BOARD_LIST_SUCCESS]: (state, {list}) => ({...state, list, loading: false}),
    [FETCH_ACTIVE_BOARD_START]: state => ({...state, loading: true}),
    [FETCH_ACTIVE_BOARD_SUCCESS]: (state, {activeBoard}) => ({...state, activeBoard, loading: false}),
    [PATCH_TASK_LIST_START]: (state) => ({
        ...state,
        loading: true
    }),
    [PATCH_TASK_LIST_FINISH]: (state, {taskList}) => ({
        ...state,
        activeBoard: {label: state.activeBoard.label, ...taskList},
        loading: false
    }),
    DEFAULT: state => state
};

export const boardReducer = (state = initialState, action) => {
    const handler = handlers[action.type] || handlers.DEFAULT;

    return handler(state, action);
};
