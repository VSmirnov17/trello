export const CREATE_BOARD = 'CREATE_BOARD';
export const FETCH_BOARD_LIST_START = 'FETCH_BOARD_LIST_START';
export const FETCH_BOARD_LIST_SUCCESS = 'FETCH_BOARD_LIST_SUCCESS';
export const FETCH_ACTIVE_BOARD_SUCCESS = 'FETCH_ACTIVE_BOARD_SUCCESS';
export const FETCH_ACTIVE_BOARD_START = 'FETCH_ACTIVE_BOARD_START';

export const PATCH_TASK_LIST_START = 'PATCH_TASK_LIST_START';
export const PATCH_TASK_LIST_FINISH = 'PATCH_TASK_LIST_FINISH';
